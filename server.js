const express = require('express');
const http = require('http');
const next = require('next');
const socketio = require("socket.io");

const port = parseInt(process.env.PORT || '3000', 10);
const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const nextHandler = nextApp.getRequestHandler();

nextApp.prepare().then(async () => {
  const app = express();
  const server = http.createServer(app);
  const io = new socketio.Server();
  io.attach(server);

  // подключение пользователя
  io.on('connection', (socket) => {
    console.log('a user connected');

    // присоединяем к комнате
    socket.on('join room', (room) => {
      const users = [];
      socket.join(room);
      // заполняем массив пользователей комнаты
      for (let id of io.sockets.adapter.rooms.get(room)) {
        users.push({
          userID: id,
          username: io.sockets.sockets.get(id).username
        });
      }
      // отправляем список всех пользователей комнаты
      socket.emit("users", users);
      // сообщаем клиенту о подключившемся к комнате пользователе 
      socket.to(room).emit("user connected", {
        userID: socket.id,
        username: socket.username,
      });
      io.to(room).emit("server message", `${socket.username} connected now!`)
    })
    // отправляем сообщение всем пользователям комнаты
    socket.on('chat message', (msg, room) => {
      io.to(room).emit('chat message', msg, socket.username);
      console.log(socket.username + ' : ' + msg);
    });
    // при отключении сообщаем об этом другим пользователям комнаты
    socket.on('disconnecting', () => {
      console.log('user disconnected');
      for (let room of socket.rooms) {
        io.to(room).emit('leave room', socket.id);
        socket.to(room).emit("server message", `${socket.username} disconnected now!`);
      }
    });
  });
  // проверяем имя на валидность
  io.use((socket, next) => {
    const username = socket.handshake.auth.username;
    if (!username) {
      return next(new Error("invalid username"));
    }
    socket.username = username;
    next();
  });

  app.all('*', (req, res) => nextHandler(req, res));

  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});