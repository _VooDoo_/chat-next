import { useEffect, useRef, useState } from "react";
import io from "socket.io-client";
import Router from 'next/router';
import styles from "../pages/index.module.css";

const socket = io('http://localhost:3000', { autoConnect: false });
export default function ChatForm() {
  // текст сообщения для отправки
  const [message, setMessage] = useState("");
  // массив всех сообщений
  const [list, setList] = useState([]);
  // проверка авторизован ли пользователь, нужно для отображения нужной страницы
  const [auth, setAuth] = useState(false);
  // проверка валидности ввожимого имени
  const [username, setUsername] = useState("");
  // массив пользователей в комнате
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // регистрация всех событий
    socket.onAny((event, ...args) => {
      console.log(event, args);
    });
    // добавление нового сообщения пользователя в массив
    socket.on('chat message', function (msg, username) {
      setList((oldlist) => {
        return [...oldlist, { "username": username, "msg": msg, "type": "user" }];
      });
    })
    // добавление нового сообщения сервера (подключился/отключился)
    socket.on('server message', function (msg) {
      setList((oldlist) => {
        return [...oldlist, { "username": "server", "msg": msg, "type": "server" }];
      });
    })
    // недопустимое имя
    socket.on("connect_error", (err) => {
      if (err.message === "invalid username") {
        setAuth(false);
      }
    });
    // обновление списка пользователей в комнате
    socket.on("users", (users) => {
      users.forEach((user) => {
        user.self = user.userID === socket.id;

      });
      // сортировка списка
      users = users.sort((a, b) => {
        if (a.self) return -1;
        if (b.self) return 1;
        if (a.username < b.username) return -1;
        return a.username > b.username ? 1 : 0;
      });
      setUsers(users);

      //если подключен не по ссылке, то создает ее
      console.log(Router.query.id)
      if (!Router.query.id) {
        Router.push({
          pathname: '/',
          query: { id: socket.id }
        });
      }
    });
    // подключение пользователя
    socket.on("user connected", (user) => {
      setUsers((oldusers) => {
        return [...oldusers, user];
      });
    });
    // отключение пользователя от комнаты
    socket.on("leave room", (userId) => {
      setUsers((oldusers) => {
        return oldusers.filter((user) => {
          return user.userID !== userId;
        })
      })
    })
  }, [])


  // если введено корректное имя, то переходит  в чат
  if (auth) {
    return (
      <div className={styles.chatPage}>
        <style jsx global>{`
          html {
            height: 100%;
          }
          body {
            padding: 0;
            margin: 0;
            height: 100%;
          }
          #__next{
            height: 100%;
        }
        `}</style>
        <div className={styles.userList}>{
          users.map((user, index) => {
            return <div className={styles.userItem} key={index}>{user.username}<span className={user.self ? styles.itMe : styles.notMe}> (You)</span></div>
          })
        }</div>
        <div className={styles.chatForm}>
          <div className={styles.messageList}>{
            list.map((msg, index) => {
              if (msg.type === "user")
                return <div className={styles.messageItem} key={index}>{`${msg.username}: ${msg.msg}`}</div>;
              else return <div className={styles.serverMessageItem} key={index}>{msg.msg}</div>
            }
            )}</div>
          <form
            className={styles.sendMessageForm}
            onSubmit={(e) => {
              e.preventDefault();
              if (message) {
                socket.emit('chat message', message, Router.query.id);
                setMessage('');
              }
            }}>
            <input
              className={styles.messageInput}
              onChange={(e) => {
                setMessage(e.target.value)
              }}
              value={message}></input>
            <button className={styles.sendMessageButton}>SEND</button>
          </form>
        </div>
      </div>)
  } else {
    return (<>
      <style jsx global>{`
          html {
            height: 100%;
          }
          body {
            padding: 0;
            margin: 0;
            height: 100%;
          }
          #__next{
            height: 100%;
        }
        `}</style>
      <form
        className={styles.usernameForm}
        onSubmit={(e) => {
          e.preventDefault();
          socket.auth = { username };
          socket.connect();
          socket.emit("join room", Router.query.id ?? socket.id);
          setAuth(true);
        }}>
        <label className={styles.usernameLabel}>Enter you name:</label>
        <input
          className={styles.usernameInput}
          placeholder="Username"
          onChange={(e) => {
            setUsername(e.target.value);
          }}
          value={username}
        ></input>
        <button className={styles.sendUsernameButton}>SEND</button>
      </form>
    </>)
  }
}